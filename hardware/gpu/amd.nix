{
  config,
  lib,
  options,
  pkgs,
  ...
}:
let
  cfg = config.stop.hardware.gpu.amd;
in
{
  options.stop.hardware.gpu.amd = {
    enable = lib.mkEnableOption "Enable the amd gpu support";
  };

  config = lib.mkIf cfg.enable {
    hardware = lib.optionalAttrs (options ? hardware.amdgpu.initrd.enable) {
      amdgpu = {
        initrd.enable = true;
        opencl.enable = true;
      };
    };

    environment.systemPackages = [
      pkgs.nvtopPackages.amd
      pkgs.rocmPackages.rocminfo
    ];
  };
}
