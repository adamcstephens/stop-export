{ config, lib, ... }:
let
  cfg = config.stop.hardware.hcloud;
in
{
  imports = [ ./disko.nix ];

  options.stop.hardware.hcloud = {
    enable = lib.mkEnableOption "hcloud hardware compatibility";

    ipv6 = lib.mkOption {
      type = lib.types.str;
      description = "ipv6 address";
    };
  };

  config = lib.mkIf cfg.enable {

    boot = {
      initrd.kernelModules = [ "virtio_gpu" ];
      kernelParams = [ "console=tty" ];

      loader = {
        systemd-boot.enable = false;
        grub = {
          enable = true;
          efiSupport = false;
          devices = [ "/dev/sda" ];
        };
      };
    };

    networking.nftables.enable = true;
    networking.useDHCP = false;
    systemd.network.enable = true;

    systemd.network.networks."10-wan" = {
      matchConfig.Name = "enp1s0";

      address = [ "${cfg.ipv6}/64" ];

      dns = [
        "1.1.1.1"
        "1.0.0.1"
        "2606:4700:4700::1111"
        "2606:4700:4700::1001"
      ];

      routes = [ { routeConfig.Gateway = "fe80::1"; } ];

      networkConfig = {
        DHCP = "ipv4";
      };
    };

  };
}
