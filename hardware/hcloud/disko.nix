{
  config,
  inputs,
  lib,
  ...
}:
let
  cfg = config.stop.hardware.hcloud;
in
{
  config = lib.mkIf cfg.enable {
    imports = [ inputs.disko.nixosModules.disko ];

    swapDevices = [
      {
        device = "/var/swapfile";
        size = 4096;
      }
    ];

    disko.devices = {
      disk = {
        sda = {
          type = "disk";
          device = "/dev/sda";
          content = {
            type = "gpt";
            partitions = {
              boot = {
                name = "boot";
                start = "0";
                end = "1M";
                label = "boot";
              };
              root = {
                start = "1M";
                end = "100%";
                label = "root";
                content = {
                  type = "btrfs";
                  extraArgs = [ "-f" ]; # Override existing partition
                  subvolumes = {
                    "/rootfs" = {
                      mountpoint = "/";
                      mountOptions = [ "compress=zstd" ];
                    };
                    "/home" = {
                      mountpoint = "/home";
                      mountOptions = [ "compress=zstd" ];
                    };
                    "/nix" = {
                      mountpoint = "/nix";
                      mountOptions = [
                        "compress=zstd"
                        "noatime"
                      ];
                    };
                    "/var" = {
                      mountpoint = "/var";
                      mountOptions = [ "compress=zstd" ];
                    };
                  };
                };
              };
            };
          };
        };
      };
    };
  };
}
